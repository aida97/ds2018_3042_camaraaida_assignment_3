# Read Me
Instalati serviciul RabbitMq si asigurati-va ca ruleaza pe portul implicit 

Pentru a rula aplicatia producer:
1. trebuie sa instalati jdk 1.8 si sa setati in path JAVA_HOME
2. instalati maven versiunea 3.0.9 si adaugati calea la PATH
3. instalati serverul tomcat si asigurati-va ca ruleaza pe portul 8080
4. descarcati proiectul
5. in terminal, navigati in folderul radacina al proiectului
6. executati comanda mvn clean install
7. porniti serverul tomcat pe portul 8080
8. deschideti un browser web si navigati la adresa https://localhost:8080/dvd.html

Pentru a rula aplicatia consumer
1. trebuie sa instalati jdk 1.8 si sa setati in path JAVA_HOME
2. instalati maven versiunea 3.0.9 si adaugati calea la PATH
3. descarcati proiectul
4. in terminal, navigati in folderul radacina al proiectului
5. executati comanda mvn clean install
6. in terminal navigati la \consumer-client\src\main\java\start
7. in terminal executati comanda java ReceiveLogs