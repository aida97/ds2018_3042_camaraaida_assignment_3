package start;

import com.rabbitmq.client.*;
import servicies.MailService;
import servicies.TextFileService;

import java.io.IOException;
import java.util.concurrent.atomic.AtomicInteger;

public class ReceiveLogs {
    private static final String EXCHANGE_NAME = "logs";

    public static void main(String[] argv) throws Exception {
        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost("localhost");
        Connection connection = factory.newConnection();
        Channel channel = connection.createChannel();

        channel.exchangeDeclare(EXCHANGE_NAME, BuiltinExchangeType.FANOUT);
        String queueName = channel.queueDeclare().getQueue();
        channel.queueBind(queueName, EXCHANGE_NAME, "");
        final MailService mailService = new MailService("aida.camara0630@gmail.com","masterverde30");

        System.out.println(" [*] Waiting for messages. To exit press CTRL+C");

        final AtomicInteger index = new AtomicInteger(0);

        Consumer consumer = new DefaultConsumer(channel) {
            @Override
            public void handleDelivery(String consumerTag, Envelope envelope,
                                       AMQP.BasicProperties properties, byte[] body) throws IOException {
                String message = new String(body, "UTF-8");
                index.incrementAndGet();
                TextFileService.writeMessage(index+"dvds.txt", message);
                System.out.println(" [x] Received '" + message + "'");
                mailService.sendMail("aida_c97@yahoo.com","notification about dvds",message);

            }
        };

        Consumer consumerGeorge = new DefaultConsumer(channel) {
            @Override
            public void handleDelivery(String consumerTag, Envelope envelope,
                                       AMQP.BasicProperties properties, byte[] body) throws IOException {
                String message = new String(body, "UTF-8");
                index.incrementAndGet();
                //TextFileService.writeMessage(index+"dvds.txt", message);
                System.out.println(" [x] Received '" + message + "'");
                mailService.sendMail("muresangeorge1996@gmail.com","notification about dvds",message);

            }
        };
        channel.basicConsume(queueName, true, consumer);
        channel.basicConsume(queueName, true, consumerGeorge);
    }

}
