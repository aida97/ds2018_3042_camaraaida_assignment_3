package servicies;


import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

public class TextFileService {
    public static void writeMessage(String fileName, String message) {

        try (BufferedWriter bw = new BufferedWriter(new FileWriter(fileName))){
            bw.write(message);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
