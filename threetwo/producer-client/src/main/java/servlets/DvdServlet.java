package servlets;

import entities.Dvd;
import start.EmitLog;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.concurrent.TimeoutException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(name = "DvdServlet")
public class DvdServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        response.setContentType("text/html");
        String title = request.getParameter("title");
        String yearString = request.getParameter("year");
        String priceString = request.getParameter("price");

        int year = Integer.parseInt(yearString);
        double price = Double.parseDouble(priceString);
        try {
            EmitLog.emitLogs(new Dvd(title,year, price));
            PrintWriter out = response.getWriter();
            String docType =
                    "<!doctype html public \"-//w3c//dtd html 4.0 " + "transitional//en\">\n";
            String htmlPage = docType+"<html>\n" +
                    "<head><title>" + "Administrator" + "</title></head>\n" +
                    "<body><h1>" + "sent" +"</h1>"+
                    "</body></html>";
            out.println(htmlPage);
        } catch (TimeoutException e) {
            System.out.println("???");
        }

    }

}
